<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class UserLeave extends Eloquent implements UserInterface, RemindableInterface {

	protected $guarded = array('id_leave', '_token');	

	protected $primaryKey = 'id_leave';

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users_leave';

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

 	public function user()
    {
        return $this->belongsTo('User');
    }	
}