<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>		
		<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
		    <tr>
		        <td align="center" valign="top">
		            <table border="0" cellpadding="20" cellspacing="0" width="600" id="emailContainer">
		                <tr>
		                    <td align="center" valign="top">
		                        <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailBody">
		                            <tr>
		                                <td align="left" valign="top">
		                                   <h2>Password Reset</h2>
		                                </td>
		                            </tr>		                        	
		                            <tr>
		                                <td align="left" valign="top">
											To reset your password, complete this form: <a href="{{ URL::to('password/reset', array($token)) }}">Reset</a>.
		                                </td>
		                            </tr>		                            
		                        </table>
		                    </td>
		                </tr>
		                <tr>
		                    <td align="center" valign="top">
		                        <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailFooter">
		                            <tr>
		                                <td align="left" valign="top">
		                                   &copy; 2013 Employee Management &amp; Leave System | Built by <a href="http://jsdecena.me">JSDecena</a>
		                                </td>
		                            </tr>
		                        </table>
		                    </td>
		                </tr>
		            </table>
		        </td>
		    </tr>
		</table>		
	</body>
</html>