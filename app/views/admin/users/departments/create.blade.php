@extends('admin.tpl.master')

@section('title')
  Create a Department - Employee Management and Leave System
@stop

@section('content')
	
		<div class="page-header">
			<h1>Create a Department</h1>
			<p>create a department.</p>
		</div>


        @if ($errors->count() > 0)
         <p>The following errors have occurred:</p>
          <ul class="alert alert-error">
              {{ $errors->first('name', '<li>:message</li>') }}
          </ul> 
        @endif 

        {{ Form::open() }}

		<div class="control-group">
			<label for="name" class="control-label">Department Name <sup class="text-error">*</sup></label>
			<div class="controls">
				{{ Form::text('name', Input::old('name'), array('id' => 'name', 'class' => 'input-xlarge', 'placeholder' => 'Department name')) }}
			</div>
		</div>

		<div class="control-group">
			<label>Set department email</label>
			<div class="controls">
				<select name="department_email" class="input-xlarge">
					@foreach ( $users as $user )
						<option value="{{ $user->email }}">{{ $user->email }}</option>
					@endforeach
				</select>
			</div>		
		</div>

		<div class="control-group">
			<label for="name" class="control-label">Department Description</label>
			<div class="controls">
				{{ Form::textarea('description', Input::old('description'), array('id' => 'description', 'class' => 'input-xlarge', 'placeholder' => 'Department description')) }}
			</div>
		</div>		

		<div class="control-group submit_button">
			<button class="btn btn-primary input-xlarge" id="department_create">Create a department</button>
		</div>

		{{ Form::close() }}
@stop