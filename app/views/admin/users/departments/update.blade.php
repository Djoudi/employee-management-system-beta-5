@extends('admin.tpl.master')

@section('title')
  Update a User Department - Employee Management and Leave System
@stop

@section('content')

		<div class="page-header">
			<h1>Update a User Department</h1>
			<p>update a user department.</p>
		</div>


        @if ($errors->count() > 0)
         <p>The following errors have occurred:</p>
          <ul class="alert alert-error">
              {{ $errors->first('name', '<li>:message</li>') }}
          </ul> 
        @endif 

        @if (Session::has('error'))
            <p class="alert alert-error"> {{ Session::get('error') }} </p>
        @elseif ( Session::has('success') )
          <p class="alert alert-success"> {{ Session::get('success') }} </p>
        @endif        

        {{ Form::open() }}

		<div class="control-group">
			<label for="name" class="control-label">Department Name <sup class="text-error">*</sup></label>
			<div class="controls">
				{{ Form::text('name', isset($records->name) ? $records->name : '', array('id' => 'name', 'class' => 'input-xlarge', 'placeholder' => 'Department name')) }}
			</div>
		</div>

		<div class="control-group">
			<label>Set department email</label>
			<div class="controls">
				<select name="department_email" class="input-xlarge">
					@foreach ( $users as $user )
						<option value="{{ $user->email }}"@if ( $user->email == $records['department_email'] ) selected="selected"@endif>{{ $user->email }}</option>
					@endforeach
				</select>
			</div>		
		</div>		

		<div class="control-group">
			<label for="name" class="control-label">Department Description</label>
			<div class="controls">
				{{ Form::textarea('description', isset($records->description) ? $records->description : '', array('id' => 'description', 'class' => 'input-xlarge', 'placeholder' => 'Department description')) }}
			</div>
		</div>		

		<div class="control-group submit_button">
			<button class="btn btn-primary input-xlarge" id="department_update">Update</button>
		</div>

		{{ Form::close() }}
@stop