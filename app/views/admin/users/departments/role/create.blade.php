@extends('admin.tpl.master')

@section('title')
  Create a User Department Role - Employee Management and Leave System
@stop

@section('content')
	
		<div class="page-header">
			<h1>Create a Department Role</h1>
			<p>create a department role.</p>
		</div>


        @if ($errors->count() > 0)
         <p>The following errors have occurred:</p>
          <ul class="alert alert-error">
              {{ $errors->first('name', '<li>:message</li>') }}
          </ul> 
        @endif

        @if (Session::has('error'))
            <p class="alert alert-error"> {{ Session::get('error') }} </p>
        @elseif ( Session::has('success') )
          <p class="alert alert-success"> {{ Session::get('success') }} </p>
        @endif         

        {{ Form::open() }}

		<div class="control-group">
			<label for="id_department" class="control-label">Department Name </label>
			<div class="controls">
				<select name="id_department" id="id_department">
					@foreach ( $records as $record)
						<option value="{{ $record->id_department }}">{{ $record->name }} </option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="control-group">
			<label for="name" class="control-label">Department Role <sup class="text-error">*</sup></label>
			<div class="controls">
				{{ Form::text('name', Input::old('name'), array('id' => 'name', 'class' => 'input-xlarge', 'placeholder' => 'Department role')) }}
			</div>
		</div>	

		<div class="control-group">
			<label for="name" class="control-label">Department Role Description</label>
			<div class="controls">
				{{ Form::textarea('description', Input::old('description'), array('id' => 'description', 'class' => 'input-xlarge', 'placeholder' => 'Department description')) }}
			</div>
		</div>

		<div class="control-group submit_button">
			<button class="btn btn-primary input-xlarge" id="department_role_create">Create a department role</button>
		</div>

		{{ Form::close() }}
@stop