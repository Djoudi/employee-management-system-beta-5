<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7 " lang="en"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7" lang="en"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8" lang="en"> <![endif]-->
<!--[if gt IE 8]> <html class="no-js ie9" lang="en"> <![endif]-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Jeff Simons Decena">

    <!-- Le styles -->
    <link href="{{ URL::asset('assets/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/docs.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/js/google-code-prettify/prettify.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/global.css') }}" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500' rel='stylesheet' type='text/css'>

    <!-- FOR DATETIME PICKER -->
    <link rel="stylesheet" type="text/css" media="screen" href="{{ URL::asset('assets/css/datepicker.css') }}">
    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>

    <link href="{{ URL::asset('assets/css/bootstrap-responsive.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('assets/js/jquery.js') }}"></script>    

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="{{ URL::asset('assets/ico/favicon.ico') }}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ URL::asset('assets/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ URL::asset('assets/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ URL::asset('assets/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ URL::asset('assets/ico/apple-touch-icon-57-precomposed.png') }}">
  </head>

  <body>
    @yield('topbar')
      <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
          <div class="container-fluid">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </a>
            <a class="brand" href="{{ URL::to('admin/dashboard') }}">E-Management System</a>
            <div class="nav-collapse collapse">
              <ul class="nav">
                <li<?php if ( Request::segment(2) == "dashboard" ) : ?> class="active"<?php endif; ?>><a href="{{ URL::to('admin/dashboard') }}">Home</a></li>
                @if ( Session::get('user_role') == 1 )
                  <li<?php if ( Request::segment(2) == "users" ) : ?> class="active"<?php endif; ?>><a href="{{ URL::to('admin/users/list') }}">Manage Users</a></li>
                  <li<?php if ( ( Request::segment(2) == "settings" && Request::segment(3) == "list" ) ) : ?> class="active"<?php endif; ?>><a href="{{ URL::to('admin/settings/list') }}">Settings</a></li>
                @endif                
                <li>
                    <a href="#contact" data-toggle="modal" data-target="#contact">Contact</a>
                </li>
                <li><a href="{{ URL::to('logout') }}">Logout</a></li>
              </ul>
            </div><!--/.nav-collapse -->
          </div>
        </div>
      </div>
    @show    

    <div class="container-fluid">
        <div class="row-fluid master-fluid">
            <div class="span3">
              <ul class="nav nav-list bs-docs-sidenav">
                <li<?php if ( Request::segment(2) == "dashboard" ) : ?> class="active"<?php endif; ?>><a href="{{ URL::to('admin/dashboard') }}"><i class="icon-chevron-right"></i> Dashboard </a></li>
                <li<?php if ( Session::get('id_user') == Request::segment(4) ) : ?> class="active"<?php endif; ?>><a href="{{ URL::to("admin/users/view") }}/{{ Session::get('id_user') }}"><i class="icon-chevron-right"></i> Your Profile </a></li>
                <li<?php if ( Request::segment(3) == "leave" && Request::segment(4) == "create" ) : ?> class="active"<?php endif; ?>><a href="{{ URL::to('admin/users/leave/create') }}"><i class="icon-chevron-right"></i> File a Leave</a></li>
                <li<?php if ( Request::segment(3) == "leave" && Request::segment(4) == "history" ) : ?> class="active"<?php endif; ?>><a href="{{ URL::to('admin/users/leave/history/') }}/{{ Session::get('id_user') }}"><i class="icon-chevron-right"></i> Your Leave history</a></li>
                  
                @if ( Session::get('user_role') == 1 )
                  
                  <li class="divider"></li>                
                  <li><a class="muted" href="javascript: void(0) "> <strong>Leave Management</strong> </a></li>
                  <li<?php if ( ( Request::segment(3) == "leave" && Request::segment(4) == "list" ) || ( Request::segment(3) == "leave" && Request::segment(4) == "update" ) ) : ?> class="active"<?php endif; ?>><a href="{{ URL::to('admin/users/leave/list') }}"><i class="icon-chevron-right"></i> Leave Applications</a></li>
                  
                  <li class="divider"></li>                
                  <li><a class="muted" href="javascript: void(0) "> <strong>User Management</strong> </a></li>
                  <li<?php if ( ( Request::segment(2) == "users" && Request::segment(3) == "list" ) || ( Request::segment(2) == "users" && Request::segment(3) == "create" ) ) : ?> class="active"<?php endif; ?>><a href="{{ URL::to('admin/users/list') }}"><i class="icon-chevron-right"></i> User List</a></li>
                  <li<?php if ( ( Request::segment(3) == "role" && Request::segment(4) == "list" ) || ( Request::segment(3) == "role" && Request::segment(4) == "update" ) || ( Request::segment(3) == "role" && Request::segment(4) == "create" )) : ?> class="active"<?php endif; ?>><a href="{{ URL::to('admin/users/role/list') }}"><i class="icon-chevron-right"></i> User Roles</a></li>                  

                  <li<?php if ( ( Request::segment(3) == "departments" && Request::segment(4) == "list" ) || ( Request::segment(3) == "departments" && Request::segment(4) == "create" ) || ( Request::segment(3) == "departments" && Request::segment(4) == "update" ) || ( Request::segment(3) == "departments" && Request::segment(4) == "user_add" ) || ( Request::segment(3) == "departments" && Request::segment(4) == "user_edit" ) || ( Request::segment(3) == "departments" && Request::segment(4) == "user_delete" )) : ?> class="active"<?php endif; ?>><a href="{{ URL::to('admin/users/departments/list') }}"><i class="icon-chevron-right"></i> Departments </a></li>

                  <li<?php if ( ( Request::segment(3) == "departments" && Request::segment(4) == "role" ) || ( Request::segment(4) == "role" && Request::segment(5) == "list" ) ) : ?> class="active"<?php endif; ?>><a href="{{ URL::to('admin/users/departments/role/list') }}"><i class="icon-chevron-right"></i> Department Roles </a></li>

                  <li class="divider"></li>                
                  <li><a class="muted" href="javascript: void(0) "> <strong>Software Settings</strong> </a></li>
                  <li<?php if ( ( Request::segment(2) == "settings" && Request::segment(3) == "list" ) ) : ?> class="active"<?php endif; ?>><a href="{{ URL::to('admin/settings/list') }}"><i class="icon-chevron-right"></i> Settings</a></li>                                  

                @endif
              </ul>              
            </div>

            <div class="span9 pull-left">
              @yield('content')
            </div>

        </div>
    </div> <!-- /container -->
    <footer class="footer">
      <div class="container-fluid">
        <p class="pull-right"><a class="scrollup" href="#">Back to top</a></p>
        <p>&copy; 2013 Employee Management System | Built by <a href="http://jsdecena.me"> JSDecena </a></p>
      </div>
    </footer>

      <div id="contact" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 id="myModalLabel">Contact Us</h3>
        </div>
        <div class="modal-body">
          <p>Created by: Jeff Simons Decena</p>
          <p>Mobile: +63 9257203071 </p>
          <p>Facebook: &nbsp; <a href="https://www.facebook.com/pages/JSDecena/119152191441482">JSDecena</a> &nbsp;  | &nbsp;  <a href="https://twitter.com/jsimonsdecena">Twitter</a></p>
          <p>Email: &nbsp;  <a href="mailto:jeff@jsdecena.me">Contact us now</a></p>
        </div>
        <div class="modal-footer">
          <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
      </div>
<!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->    
    <script src="{{ URL::asset('assets/js/bootstrap-transition.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootstrap-alert.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootstrap-modal.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootstrap-dropdown.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootstrap-scrollspy.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootstrap-tab.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootstrap-tooltip.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootstrap-popover.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootstrap-button.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootstrap-collapse.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootstrap-carousel.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootstrap-typeahead.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {

          //IF USER CREATE / UPDATE PAGE
          @if ( Request::segment(3) == "create" || Request::segment(3) == "update" )

              $('.date').datepicker();

          @elseif ( 
                    ( Request::segment(3) == "leave" && Request::segment(4) == "create" ) || 
                    ( Request::segment(3) == "leave" && Request::segment(4) == "update" ) 
                  )

              $('#day_leave_type').on('change',function(){

                  if( $(this).val() == "halfday" ) {

                      $('#leave_time_wrap').slideDown().removeClass('hide');
                      $('#leave_time').attr('disabled', false);
                      $('#day_leave').attr('disabled', false);
                      $('#multiple_days').slideUp().addClass('hide');
                      $('#date_to').attr('data-status', 'disabled');

                  }else if ( $(this).val() == "oneday" ) {
                      
                      //UN DISABLE THE LEAVE TIME AND SHOW THE WRAP
                      $('#leave_time').attr('disabled', true);
                      $('#leave_time_wrap').slideUp().addClass('hide');

                      //SET THE NUMBER OF DAYS
                      $('#single_day').slideDown().removeClass('hide');
                      $('#day_leave').attr('disabled', false);
                      $('#multiple_days').slideUp().addClass('hide');
                      $('#date_to').attr('data-status', 'disabled');

                  }else if ( $(this).val() == "multiple" ) {

                      $('#leave_time').attr('disabled', true);
                      $('#multiple_days').slideDown().removeClass('hide');
                      $('.multiple_days_input').attr('disabled', false);
                      $('#single_day').slideUp().addClass('hide');
                      $('#date_to').attr('data-status', 'enabled');

                  }else{
                      $('#leave_time').attr('disabled', true);
                      $('#leave_time_wrap').slideUp().addClass('hide');
                      $('#multiple_days').slideUp().addClass('hide');
                      $('#date_to').attr('data-status', 'disabled');
                  }
              });

              $('#leave_time').on('change',function(){
                
                  if( $(this).val() != '#noSelect' ){                  
                      $('#single_day').slideDown().removeClass('hide');
                      $('#day_leave').attr('disabled', false);
                  }else{
                      $('#day_leave').attr('disabled', true);
                      $('#single_day').slideUp('fast').addClass('hide');
                      $('#multiple_days').slideUp().addClass('hide');
                  }

              });

              //FOR THE REQUEST LEAVE PICKER
              $('.date').datepicker().on('changeDate', function(){

                  if ( Date.parse( $('#date_from').val() ) >= Date.parse( $('#date_to').val() ) ){
                      $('#alert').fadeIn().find('strong').text('The "Date From" cannot be greater than or equal the "Date To".');
                      $('button.btn').attr('disabled', true);
                  }else{

                      $('button.btn').attr('disabled', false);
                      $('#alert').fadeOut();
                  }
              });                                       

          @endif

              $('.scrollup').click(function(){
                  $("html, body").animate({ scrollTop: 0 }, 600);
                  return false;
              });

              //ANIMATION FOR THE TYPE OF LEAVE
              $('#leave_type').change(function(){
                if ( $(this).val() == "other") {
                  $('.other_reason').slideDown().removeClass('hidden');
                }else{
                  $('.other_reason').slideUp().addClass('hidden');
                }               
              });

              //FADEOUT ALERTS AFTER SHOWING
              $('.alert').delay(2000).slideUp();
        });


        function daydiff(first, second) {
            return (second-first)/(1000*60*60*24)
        }
      </script>                
  </body>
</html>