<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//============ GET PUBLIC ============ //
Route::get('/', 							'AdminController@index');
Route::get('login', 						'AdminController@login');
Route::get('logout',						'AdminController@logout');
Route::get('password/remind',				'PublicController@password_remind');

//============ CREATING A FILTER FOR ALL THE ADMIN LOCATIONS ============ //
Route::group(array('before' => 'auth'), function()
{	
	//============ GET ADMIN ============ //
	Route::get('admin', 															'AdminController@dashboard');
	Route::get('admin/dashboard', 													'AdminController@dashboard');
	Route::get('admin/settings/list', 												'AdminController@settings');
	Route::get('admin/users/list', 													'UsersController@index');
	Route::get('admin/users/create', 												'UsersController@usersCreate');
	Route::get('admin/users/view/{id_user}', 										'UsersController@usersView');
	Route::get('admin/users/update/{id_user}', 										'UsersController@usersUpdate');
	Route::get('admin/users/delete/{id_user}', 										'UsersController@usersDelete');
	Route::get('admin/users/role', 													'UsersController@usersRoleList');
	Route::get('admin/users/role/list', 											'UsersController@usersRoleList');
	Route::get('admin/users/role/create', 											'UsersController@usersRoleCreate');
	Route::get('admin/users/role/update/{id_role}', 								'UsersController@usersRoleUpdate');
	Route::get('admin/users/role/delete/{id_role}', 								'UsersController@usersRoleDelete');
	Route::get('admin/users/leave/create', 											'LeaveController@index');
	Route::get('admin/users/leave/list', 											'LeaveController@leaveList');
	Route::get('admin/users/leave/update/{id_user}', 								'LeaveController@leaveUpdate');
	Route::get('admin/users/leave/history', 										'LeaveController@history');
	Route::get('admin/users/leave/history/{id_user}', 								'LeaveController@historyUser');
	Route::get('admin/users/departments', 											'UsersController@usersDepartmentList');
	Route::get('admin/users/departments/list', 										'UsersController@usersDepartmentList');
	Route::get('admin/users/departments/create', 									'UsersController@usersDepartmentCreate');
	Route::get('admin/users/departments/update/{id_department}', 					'UsersController@usersDepartmentUpdate');
	Route::get('admin/users/departments/delete/{id_department}', 					'UsersController@usersDepartmentDelete');
	Route::get('admin/users/departments/role/list', 								'UsersController@usersDepartmentRoleList');
	Route::get('admin/users/departments/role/create', 								'UsersController@usersDepartmentRoleCreate');
	Route::get('admin/users/departments/role/update/{id_department_role}',			'UsersController@usersDepartmentRoleUpdate');
	Route::get('admin/users/departments/user_add/{id_department}', 					'UsersController@usersDepartmentUsersAdd');
	Route::get('admin/users/departments/user_edit/{id_user}/{id_department}/{id_department_role}', 		'UsersController@usersDepartmentUsersEdit');
	Route::get('admin/users/departments/user_delete/{id_user}/{id_department}/{id_department_role}/{id_department_user}', 		'UsersController@usersDepartmentUsersDelete');

	//============ POST METHOD IN ADMIN ============ //

		//============ POST CREATE METHOD ============ //
		Route::post('admin/users/create', 			array('before' => 'csrf', function(){

			$rules = array('first_name' => 'required', 'password' => 'required', 'email' => 'email|required|unique:users,email');

		    $validator = Validator::make(Input::all(), $rules);

		    if ($validator->fails()) {
		    
		    	//IF VALIDATION FAILS, REDIRECT TO LOGIN PAGE WITH THE ERRORS
		        return Redirect::to('admin/users/create')->withErrors($validator)->withInput();
		    
		    }else{
				
				//IF VALIDATION SUCCEED INSERT POST TO DATABASE
		        User::create(array(
		        	'id_role' 				=> Input::get('id_role'),
		        	'department_name' 		=> Input::get('department_name'),
		        	'department_role_name' 	=> Input::get('department_role_name'),
		        	'title' 				=> Input::get('title'),
					'first_name' 			=> Input::get('first_name'),
					'middle_name' 			=> Input::get('middle_name'),
					'last_name' 			=> Input::get('last_name'),
					'email' 				=> Input::get('email'),
					'password' 				=> Hash::make(Input::get('password')),
					'mobile' 				=> Input::get('mobile'),
					'tel' 					=> Input::get('tel'),
					'street' 				=> Input::get('street'),
					'town_city' 			=> Input::get('town_city'),
					'postal' 				=> Input::get('postal'),
					'country' 				=> Input::get('country'),
					'company_name' 			=> Input::get('company_name'),
					'job_scope' 			=> Input::get('job_scope'),
					'employment_date' 		=> Input::get('employment_date'),
					'employment_date_end' 	=> '',
					'salary' 				=> Input::get('salary'),
					'bank' 					=> Input::get('bank'),
					'account_name' 			=> Input::get('account_name'),
					'account_number' 		=> Input::get('account_number'),
					'account_type' 			=> Input::get('account_type'),
					'others' 				=> Input::get('others'),
					'currency' 				=> Input::get('currency'),
					'annual_leave' 			=> Input::get('annual_leave'),
					'sick_leave' 			=> Input::get('sick_leave'),
					'maternity_leave' 		=> Input::get('maternity_leave'),
					'paternity_leave' 		=> Input::get('paternity_leave'),
					'status' 				=> Input::get('status'),
					'created_at'			=> date("Y-m-d"),
					'updated_at'			=> date("Y-m-d")
		        ));

		        return Redirect::to('admin/users/list')->with('success', 'You have successfully created a user.');
			}

		}));

		
		//============ POST UPDATE USER METHOD ============ //
		Route::post('admin/users/update/{id_user}', array('before' => 'csrf', function($id_user)
		{
			$rules = array('first_name' => 'required', 'email' => 'email|required');

		    $validator = Validator::make(Input::all(), $rules);

		    if ($validator->fails()) {
		    
		    	//IF VALIDATION FAILS, REDIRECT TO LOGIN PAGE WITH THE ERRORS
		        return Redirect::to("admin/users/update/$id_user")->withErrors($validator)->withInput();
		    
		    }else{

				//IF VALIDATION SUCCEED INSERT POST TO DATABASE		
				User::where('id_user', '=', $id_user)->update(Input::except('_token', 'department_name', 'department_role_name'));

				//UPDATE THE DEPARTMENT USERS TABLE
				DepartmentUsers::where('id_user', '=', $id_user)->update(Input::only('department_name', 'department_role_name') );

				return Redirect::to("admin/users/update/$id_user")->with('success', 'You have successfully edited this user.');		     
			}
		}));

		//============ POST UPDATE USER PASSWORD METHOD ============ //
		Route::get('admin/users/update/password/{id_user}', 						'UsersController@passwordUpdate');
		Route::post('admin/users/update/password/{id_user}', array('before' => 'csrf', function($id_user)
		{
			$rules = array('password' => 'required|min:8');

		    $validator = Validator::make(Input::all(), $rules);

		    if ($validator->fails()) {
		    
		    	//IF VALIDATION FAILS, REDIRECT TO LOGIN PAGE WITH THE ERRORS
		        return Redirect::to("admin/users/update/password/$id_user")->withErrors($validator)->withInput();
		    
		    }else{

				//IF VALIDATION SUCCEED INSERT POST TO DATABASE		
				$user = User::find(Request::segment(5));

				$user->password = Hash::make(Input::get('password'));

				$user->save();

				return Redirect::to("admin/users/update/password/$id_user")->with('success', 'You have successfully changed this user\'s password!');
			}
		}));

		//============ POST DELETE USER METHOD ============ //
		Route::post('admin/users/delete/{id_user}', array('before' => 'csrf', function($id_user)
		{
			//CHECK IF NOT THE LAST USER, DO DELETE OTHERWISE PROHIBIT FROM DELETING IT
			if ( User::where('id_user', '>', 1 )->count() ){

				$user = User::find(Request::segment(4));
				$user->delete();

				return Redirect::to("admin/users/list")->with('message', 'You have successfully deleted a user.');
			
			}else{

				return Redirect::to("admin/users/list")->with('message', 'Sorry, this is the last user and you cannot delete it.');

			}
		}));

		//============ POST CREATE ROLE METHOD ============ //
		Route::post('admin/users/role/create', array('before' => 'csrf', function()
		{
			$rules = array('role' => 'required');

		    $validator = Validator::make(Input::all(), $rules);

		    if ($validator->fails()) {
		    
		    	//IF VALIDATION FAILS, REDIRECT TO LOGIN PAGE WITH THE ERRORS
		        return Redirect::to('admin/users/role/create')->withErrors($validator)->withInput();
		    
		    }else{

				//IF VALIDATION SUCCEED INSERT POST TO DATABASE
				UserRole::create(Input::all());

		        return Redirect::to('admin/users/role/list')->with('success', 'You have successfully created a user role.');
			}
		}));

		//============ POST UPDATE ROLE METHOD ============ //
		Route::post('admin/users/role/update/{id_role}', array('before' => 'csrf', function($id_role)
		{
			$rules = array('role' => 'required');

		    $validator = Validator::make(Input::all(), $rules);

		    if ($validator->fails()) {
		    
		    	//IF VALIDATION FAILS, REDIRECT TO LOGIN PAGE WITH THE ERRORS
		        return Redirect::to("admin/users/role/update/$id_role")->withErrors($validator)->withInput();
		    
		    }else{

				//IF VALIDATION SUCCEED INSERT POST TO DATABASE		
				UserRole::where('id_role', '=', $id_role)->update(Input::all());

				return Redirect::to("admin/users/role/update/$id_role")->with('success', 'You have successfully edited a user role.');		     
			}
		}));

		//============ POST DELETE ROLE METHOD ============ //
		Route::post('admin/users/role/delete/{id_role}', array('before' => 'csrf', function($id_role)
		{
			//CHECK IF NOT THE LAST USER ROLE, DO DELETE OTHERWISE PROHIBIT FROM DELETING IT
			if ( User::where('id_role', '>', 1 )->count() ){

				$user = UserRole::find(Request::segment(5));
				$user->delete();
			
			}else{
				
				return Redirect::to('admin/users/role/list')->with('error', 'Ooops, this is the last use role and cannot be deleted.');

			}

			return Redirect::to("admin/users/role/list")->with('success', 'You have successfully deleted a user role.');
		}));

		//============ POST LEAVE CREATE METHOD ============ //
		Route::post('admin/users/leave/create', array('before' => 'csrf', function(){

			$rules = array('leave_details' => 'required');

		    $validator = Validator::make(Input::all(), $rules);

		    if ($validator->fails()) {
		    
		    	//IF VALIDATION FAILS, REDIRECT TO CREATE PAGE
		        return Redirect::to('admin/users/leave/create')->withErrors($validator)->withInput();
		    
		    }else{

		    	if ( Input::get('day_leave_type') == "halfday" ){

					$data = array(
						'id_user'			=> Session::get('id_user'),
						'first_name'		=> Session::get('first_name'),
						'last_name'			=> Session::get('last_name'),
						'leave_type' 		=> Input::get('leave_type'),
						'other_reason' 		=> Input::get('other_reason'),
						'day_leave_type' 	=> Input::get('day_leave_type'),
						'leave_time' 		=> Input::get('leave_time'),
						'day_leave'			=> Input::get('day_leave'),
						'date_from' 		=> null,
						'date_to' 			=> null,
						'number_of_days' 	=> 0.5,
						'leave_details' 	=> Input::get('leave_details'),
						'created_at'		=> date("Y-m-d H:i:s"),
						'updated_at'		=> date("Y-m-d H:i:s")
					);		    		

		    	}elseif ( Input::get('day_leave_type') == "oneday" ){

					$data = array(
						'id_user'			=> Session::get('id_user'),
						'first_name'		=> Session::get('first_name'),
						'last_name'			=> Session::get('last_name'),
						'leave_type' 		=> Input::get('leave_type'),
						'other_reason' 		=> Input::get('other_reason'),
						'day_leave_type' 	=> Input::get('day_leave_type'),
						'leave_time' 		=> Input::get('leave_time'),
						'day_leave'			=> Input::get('day_leave'),
						'date_from' 		=> null,
						'date_to' 			=> null,
						'number_of_days' 	=> 1,
						'leave_details' 	=> Input::get('leave_details'),
						'created_at'		=> date("Y-m-d H:i:s"),
						'updated_at'		=> date("Y-m-d H:i:s")
					);		    		

		    	}else{
					
					$data = array(
						'id_user'			=> Session::get('id_user'),
						'first_name'		=> Session::get('first_name'),
						'last_name'			=> Session::get('last_name'),
						'leave_type' 		=> Input::get('leave_type'),
						'other_reason' 		=> Input::get('other_reason'),
						'day_leave_type' 	=> Input::get('day_leave_type'),
						'leave_time' 		=> Input::get('leave_time'),
						'date_from' 		=> Input::get('date_from'),
						'date_to' 			=> Input::get('date_to'),
						'number_of_days' 	=> getWorkingDays(date("F j, Y", strtotime(Input::get('date_from'))),date("F j, Y", strtotime(Input::get('date_to')))),
						'leave_details' 	=> Input::get('leave_details'),
						'created_at'		=> date("Y-m-d H:i:s"),
						'updated_at'		=> date("Y-m-d H:i:s")
					);		    		
		    	}

		    	//CHECK IF THERE IS SOMETHING IS SET IN THE SETTINGS TABLE
		    	if ( isset( Settings::find(1)->value ) && Settings::find(1)->value != NULL ) {
					//SEND EMAIL TO THE APPROPRIATE PERSON FOR APPROVAL			
					Mail::send('emails.requestPending', $data, function($message) use ($data)
					{
						$message->from(Session::get('email'), $data['first_name'] .' '. $data['last_name'] );
					    $message->to(Settings::find(1)->value, 'ADMIN')->cc(Session::get('email'), $data['first_name'] .' '. $data['last_name'])->subject('Leave Pending Approval'); //THE OVERALL EMAIL OPTION

					});	
		    	}			

				//IF VALIDATION SUCCEED INSERT POST TO DATABASE
				DB::table('users_leave')->insert($data);

				return Redirect::to('admin/users/leave/create')->with('success', 'You have successfully created a leave request.');
			}					

		}));

		//============ POST LEAVE UPDATE METHOD ============ //
		Route::post('admin/users/leave/update/{id_leave}', array('before' => 'csrf', function($id_leave){

			$id_leave = Request::segment(5);

			$rules = array('leave_details' => 'required', 'approval' => 'required');

		    $validator = Validator::make(Input::all(), $rules);

		    if ($validator->fails()) {
		    
		    	//IF VALIDATION FAILS, REDIRECT TO CREATE PAGE
		        return Redirect::to("admin/users/leave/update/$id_leave")->withErrors($validator)->withInput();
		    
		    }else{

				//SEND EMAIL TO THE APPROPRIATE PERSON FOR APPROVAL
				$data = array(
					'leave_type' 		=> Input::get('leave_type'),
					'other_reason' 		=> Input::get('other_reason'),
					'day_leave_type' 	=> Input::get('day_leave_type'),
					'leave_time' 		=> Input::get('leave_time'),
					'date_from' 		=> Input::get('date_from'),
					'date_to' 			=> Input::get('date_to'),
					'number_of_days' 	=> Input::get('number_of_days'),
					'leave_details' 	=> Input::get('leave_details'),
					'approved_by' 		=> Input::get('approved_by'),
					'approved_date' 	=> Input::get('approved_date'),
					'notes' 			=> Input::get('notes'),
					'created_at'		=> date("Y-m-d H:i:s"),
					'updated_at'		=> date("Y-m-d H:i:s")					
				);

				if ( Input::get('approval') == 1 ) {
					
					//EMAIL APPROVED
					Mail::send('emails.requestApproved', $data, function($message) use ($data)
					{
						$message->from(Settings::find(1)->value, 'ADMIN');
					    $message->to( Session::get('email'), Session::get('first_name') .' '. Session::get('last_name') )->subject('Leave Approved')
					    	->cc(Settings::find(1)->value, 'ADMIN');
					});
				
				}else{

					//EMAIL DISAPPROVED
					Mail::send('emails.requestDisapproved', $data, function($message) use ($data)
					{
						$message->from(Settings::find(1)->value, 'Admin');
					    $message->to(Session::get('email'), Session::get('first_name') .' '. Session::get('last_name') )->subject('Leave Disapproved')
					    	->cc(Settings::find(1)->value, 'ADMIN');
					});

				}

				//IF VALIDATION SUCCEED UPDATE POST TO DATABASE
				UserLeave::where('id_leave', '=', Request::segment(5) )->update(Input::except('_token', 'email'));

				return Redirect::to('admin/users/leave/list')->with('success', 'You have successfully updated a leave request.');
			}
		}));

		//============ POST DEPARTMENT CREATE METHOD ============ //
		Route::post('admin/users/departments/create', array('before' => 'csrf', function(){

			$rules = array('name' => 'required');

		    $validator = Validator::make(Input::all(), $rules);

		    if ($validator->fails()) {
		    
		    	//IF VALIDATION FAILS, REDIRECT TO CREATE PAGE
		        return Redirect::to("admin/users/departments/create")->withErrors($validator)->withInput();
		    
		    }else{

		    	Departments::create(Input::all());

		    	return Redirect::to('admin/users/departments/list')->with('success', 'You have successfully created a department');
		    }			

		}));

		//============ POST UPDATE DEPARTMENT METHOD ============ //
		Route::post('admin/users/departments/update/{id_department}', array('before' => 'csrf', function($id_department)
		{			
			$id_department = Request::segment(5);

			$rules = array('name' => 'required');

		    $validator = Validator::make(Input::all(), $rules);

		    if ($validator->fails()) {
		    
		    	//IF VALIDATION FAILS, REDIRECT TO UPDATE PAGE WITH THE ERRORS
		        return Redirect::to("admin/users/departments/update/$id_department")->withErrors($validator)->withInput();
		    
		    }else{

				//IF VALIDATION SUCCEED INSERT POST TO DATABASE		
				Departments::where('id_department', '=', $id_department)->update(Input::all());

				return Redirect::to("admin/users/departments/update/$id_department")->with('success', 'You have successfully edited a user role.');		     
			}
		}));

		//============ POST DELETE DEPARTMENT METHOD ============ //
		Route::post('admin/users/departments/delete/{id_department}', array('before' => 'csrf', function($id_department)
		{			
			$id_department = Request::segment(5);

			if ( Departments::where('id_department', '>', 1 )->count() ){

				$department = Departments::find(Request::segment(5));
				$department->delete();

				return Redirect::to("admin/users/departments/list")->with('success', 'You have successfully deleted a department.');
			
			}else{

				return Redirect::to("admin/users/departments/list")->with('error', 'Sorry, this is the lastdepartment and you cannot delete it.');

			}			

		}));

		//============ POST UPDATE DEPARTMENT ROLE METHOD ============ //
		Route::post('admin/users/departments/role/update/{id_department_role}', array('before' => 'csrf', function($id_department_role)
		{			
			$id_department_role = Request::segment(6);

			$rules = array('name' => 'required');

		    $validator = Validator::make(Input::all(), $rules);

		    if ($validator->fails()) {
		    
		    	//IF VALIDATION FAILS, REDIRECT TO UPDATE PAGE WITH THE ERRORS
		        return Redirect::to("admin/users/departments/role/update/$id_department_role")->withErrors($validator)->withInput();
		    
		    }else{
		    	
				//IF VALIDATION SUCCEED INSERT POST TO DATABASE		
				DepartmentRoles::where('id_department_role', '=', $id_department_role)->update(Input::all());

				return Redirect::to("admin/users/departments/role/update/$id_department_role")->with('success', 'You have successfully edited a department role.');		     
			}
		}));

		Route::post('admin/users/departments/role/create', array('before' => 'csrf', function()
		{
			$rules = array('name' => 'required');

		    $validator = Validator::make(Input::all(), $rules);

		    if ($validator->fails()) {
		    
		    	//IF VALIDATION FAILS, REDIRECT TO UPDATE PAGE WITH THE ERRORS
		        return Redirect::to("admin/users/departments/role/create")->withErrors($validator)->withInput();
		    
		    }else{

				//IF VALIDATION SUCCEED INSERT POST TO DATABASE
				DepartmentRoles::create(Input::all());

				return Redirect::to("admin/users/departments/role/list")->with('success', 'You have successfully created a department role.');
			}			

		}));

		//ADDING OF DEPARTMENT
		Route::post('admin/users/departments/add/{id_department}', array('before' => 'csrf', function($id_department)
		{
			$id_department = Request::segment(5);

			User::where('id_user', '=', Input::get('id_user'))->update(Input::except('_token', 'name'));

			return Redirect::to("admin/users/departments/add/{id_department}")->with('success', 'You have successfully added this user.');

		}));

		//ADDING OF USER IN THE DEPARTMENT
		Route::post('admin/users/departments/user_add/{id_department}', array('before' => 'csrf', function($id_department)
		{
			$id_department = Request::segment(5);

			$rules = array('name' => 'unique:department_users,name');

		    $validator = Validator::make(Input::all(), $rules);

		    if ($validator->fails()) {

		    	return Redirect::to("admin/users/departments/user_add/$id_department")->with('error', 'Ooops, this user is already existing. Please add this user to other department.');

		    }else{
				
				//INSERT POST TO DEPARTMENT USERS TABLE
				DepartmentUsers::create(Input::all());

				//UPDATE POST IN USERS TABLE
				User::where('id_user', '=', Input::get('id_user'))->update(Input::only('department_name', 'department_role_name'));

				return Redirect::to("admin/users/departments/list")->with('success', 'You have successfully added a user.');

		    }

		}));	

		Route::post('admin/users/departments/user_edit/{id_user}/{id_department}/{id_department_role}', array('before' => 'csrf', function($id_user){

			$id_user = Request::segment(5);

			DepartmentUsers::where('id_user', '=', $id_user)->update(Input::all());

			return Redirect::to("admin/users/departments/list")->with('success', 'You have successfully edited a user.');
		}));

		Route::post('admin/users/departments/user_delete/{id_user}/{id_department}/{id_department_role}/{id_department_user}', array('before' => 'csrf', function($id_department_user){

			$id_department_user = Request::segment(8);

			$id_department_user = DepartmentUsers::find($id_department_user);
			$id_department_user->delete();

			//UPDATE POST IN USERS TABLE
			User::where('id_user', '=', Request::segment(5) )->update(Input::only('department_name', 'department_role_name'));			

			return Redirect::to("admin/users/departments/list")->with('success', 'You have successfully deleted a user in their Department/Role.');
		}));

		Route::post('admin/settings/list', array('before' => 'csrf', function(){
			
			$rules = array('LEAVE_EMAIL' => 'required|email');

		    $validator = Validator::make(Input::all(), $rules);

		    if ($validator->fails()) {

		    	return Redirect::to("admin/settings/list")->withErrors($validator)->withInput();

		    }else{		    	

		    	$settings = Settings::all();

		    	foreach ( $settings as $setting ) {
					
					if ( $setting['name'] == "LEAVE_EMAIL" ) :
						$set = Settings::find( $setting['id_setting']);
						$set->value = Input::get('LEAVE_EMAIL');
						$set->save();					
					endif;
		    	}

				return Redirect::to("admin/settings/list")->with('success', 'You have successfully updated the settings.');

		    }			

		}));

}); //============ END POST METHOD IN ADMIN ============ //


//============ END CREATING A FILTER FOR ALL THE ADMIN LOCATIONS ============ //


//============ POST PUBLIC ============ //
Route::post('login', array('before' => 'csrf', function(){
		
		$rules = array('email' => 'email|required' , 'password' => 'required|min:8');

	    $validator = Validator::make(Input::all(), $rules);

	    if ($validator->fails()) {
	    
	    	//IF VALIDATION FAILS, REDIRECT TO LOGIN PAGE WITH THE ERRORS
	        return Redirect::to('/')->withErrors($validator)->withInput();
	    
	    }else{

			//ATTEMPT LOGIN
			if (Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password'), 'status' => '1' ))) {
				
				$user = DB::table('users')->where('email', Input::get('email'))->first();

				Session::put('user_role', $user->id_role);
				Session::put('id_user', $user->id_user);
				Session::put('email', $user->email);				
				Session::put('first_name', $user->first_name);
				Session::put('last_name', $user->last_name);

				//IF SUCCESSFUL, REDIRECT TO THE ADMIN    
			    return Redirect::intended('admin/dashboard');
			
			}else{

				//IF UNSUCCESSFUL, REDIRECT BACK TO THE LOGIN
				return Redirect::to('/')->with('message', 'Email and password do not match or account is disabled. Please contact the admin.');
			}
		}	
}));

Route::post('password/remind', array('before' => 'csrf', function(){

	//CHECK FOR THE EMAIL / USER IF IT EXIST
	$user = DB::table('users')->where('email', Input::get('email'))->first();


	if( count($user) > 0 ) {

		//INSERT A HASHED TOKEN TO THE USER REMINDER TABLE
		DB::table('password_reminders')->insert(
		    array('email' => Input::get('email'), 'token' => Input::get('_token'), 'created_at' => date('Y-m-d') )
		);

		$data = array('email' => Input::get('email'), 'token' => Input::get('_token'));

		Mail::send('emails/auth/reminder', $data, function($message) use ($data)
		{
			$message->from('admin@ems.com');
		    $message->to($data['email'])->subject('Reset your password');

		});		

		return Redirect::intended('password/remind')->with('success', 'An email has been sent with the instruction to reset your password.');		

	}else{

		return Redirect::intended('password/remind')->with('error', 'Sorry, we cannot find your email in our system.');

	}

}));

//RESET GET
Route::get('password/reset/{token}', function($token)
{
	$token = Request::segment(3);

	//CHECK FOR VALID TOKEN IN THE REMINDERS TABLE
	$valid = DB::table('password_reminders')->where('token', $token)->first();

	if ( count($valid) > 0 ){
	
		return View::make('password/reset')->with('token', $token);
	
	}else{

		return Redirect::to('/')->with('error', 'Sorry, not valid credential. Please click the valid link in your email.');		
	}

});

//RESET POST
Route::post('password/reset/{token}', function($token)
{

	//VALIDATE USER INPUT
	$rules = array('email' => 'email|required' , 'password' => 'required|min:8', 'password_confirmation' => 'required|same:password');

    $validator = Validator::make(Input::all(), $rules);

    if ($validator->fails()) {
    
    	//IF VALIDATION FAILS, REDIRECT TO LOGIN PAGE WITH THE ERRORS
        return Redirect::intended("password/reset/$token")->withErrors($validator)->withInput();
    
    }else{

    	//CHECK FOR THE USER EXISTENCE
    	$valid_email = DB::table('users')->where('email', Input::get('email'))->first();

    	if ( count($valid_email) > 0 ){

    		//IF VALID USER, CREATE ANOTHER PASSWORD FOR THEM
    		User::where('email', '=', Input::get('email'))->update(array('password' => Hash::make(Input::get('password'))));

    		//DELETE THE REMINDER ROW
    		DB::table('password_reminders')->where('email', '=', Input::get('email'))->delete();

    		return Redirect::intended("/")->with('success', 'You have successfully reset the password. Please login again.');	    	

    	}else{

			//IF NOT VALID EMAIL, REDIRECT TO LOGIN PAGE WITH THE ERRORS
	        return Redirect::intended("password/reset/$token")->with('error', 'Sorry we do not recognize this email. Please try again.');

    	}

    }
});

function getWorkingDays($startDate,$endDate){
	//The function returns the no. of business days between two dates and it skips the holidays

    // do strtotime calculations just once
    $endDate = strtotime($endDate);
    $startDate = strtotime($startDate);


    //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
    //We add one to inlude both dates in the interval.
    $days = ($endDate - $startDate) / 86400 + 1;

    $no_full_weeks = floor($days / 7);
    $no_remaining_days = fmod($days, 7);

    //It will return 1 if it's Monday,.. ,7 for Sunday
    $the_first_day_of_week = date("N", $startDate);
    $the_last_day_of_week = date("N", $endDate);

    //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
    //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
    if ($the_first_day_of_week <= $the_last_day_of_week) {
        if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
        if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
    }
    else {
        // (edit by Tokes to fix an edge case where the start day was a Sunday
        // and the end day was NOT a Saturday)

        // the day of the week for start is later than the day of the week for end
        if ($the_first_day_of_week == 7) {
            // if the start date is a Sunday, then we definitely subtract 1 day
            $no_remaining_days--;

            if ($the_last_day_of_week == 6) {
                // if the end date is a Saturday, then we subtract another day
                $no_remaining_days--;
            }
        }
        else {
            // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
            // so we skip an entire weekend and subtract 2 days
            $no_remaining_days -= 2;
        }
    }

    //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
	//---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
   $workingDays = $no_full_weeks * 5;
	if ($no_remaining_days > 0 )
    {
      $workingDays += $no_remaining_days;
    }   

    return $workingDays;
}